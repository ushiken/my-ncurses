#include <ncurses.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

// オブジェクトの構造体
struct OBJECT {
    enum OBJ { WLL, GRD } objType;
    enum O_STATUS { WAIT, MOVE, END } status;
    int X;
    int Y; // オブジェクトの座標
    struct OBJECT *next; // 次のオブジェクトへのポインタ
    struct OBJECT *head; // 最初のオブジェクトへのポインタ
};

// プレイヤーの構造体
struct PLAYER {
    enum P_STATUS { RUN, UP, AIR, DOWN, LAND } status;
    int X;
    int Y;
    int diff;
    char name[100];
};

// ランキングの構造体
struct RANK {
    char name[100];
    int score;
    int diff;
};

// メモリの解放
void freeStruct(struct OBJECT *obj, struct PLAYER *ply) {
    struct OBJECT *p=NULL;
    do {
        p = obj->next;
        free(obj);
        obj = p;
    } while (p != NULL);
    free(ply);
}

// オブジェクトの作成
void makeObject(struct OBJECT **obj, int x, int y, enum OBJ objType) {
    struct OBJECT *ptr, *new;
    new = (struct OBJECT *)malloc(sizeof(struct OBJECT));
    if (*obj == NULL) {
        *obj = new;
        new->status = MOVE;
        new->head = *obj;
    } else {
        ptr = *obj;
        while (ptr->next != NULL) {
            ptr = ptr->next;
        }
        ptr->next = new;
        new->status = WAIT;
        new->head = ptr->head;
    }

    new->X = x;
    new->Y = y;
    new->objType = objType;
    new->next = NULL;
}

// ステージの作成
void makeStage(struct OBJECT **obj, int len) {
    int x, y=LINES/2, rnd;
    len = COLS-1 + len;
    for (x=COLS-1; x<len; x++) {
    // 約10%の確率で壁のオブジェクトを生成
        rnd = rand() % 100 + 1;
        if (rnd % 10 == 0) makeObject(obj, x, y, WLL);
        else makeObject(obj, x, y, GRD);
    }
}

// ステージの初期化
void initStage() {
    int i, y=LINES/2;
    char *grd = "_", *body = "^O";
    for (i=0; i<COLS; i++) {
        if (i == COLS/4) {
            mvaddch(y, COLS/4, body[0]);
            mvaddch(y-1, COLS/4, body[1]);
        } else mvaddch(y, i, grd[0]);
    }
}


// プレイヤーの初期化
void initPlayer(struct PLAYER *ply) {
    ply->X = COLS/4;
    ply->Y = LINES/2;
    ply->status = RUN;
    // プレイヤー名入力
    echo();
    mvprintw(LINES/2-8, COLS/2-10, "WELCOME TO SCROLL");
    mvprintw(LINES/2-7, COLS/2-10, "TYPE YOUR NAME:");
    getnstr(ply->name, 100);
    noecho();
    refresh();
}

// オブジェクトの初期化
void initObject(struct OBJECT *obj, struct OBJECT **p) {
    obj = NULL;
    *p = &(*obj);
}

// ステージのスクロール
void scrollStage(struct OBJECT *obj) {
    char *grd = "_", *wll = "|";
    while (obj->next != NULL) {
        if (obj->status == MOVE) {        // オブジェクトが移動状態なら
            if (obj->objType == WLL) mvaddch(obj->Y, obj->X, wll[0]);
            else mvaddch(obj->Y, obj->X, grd[0]);
            obj->X = obj->X-1;
            if (obj->X < 0) obj->status = END; // オブジェクトが画面端より左に行ったら
            obj = obj->next;
        } else if (obj->status == WAIT) {// オブジェクトが待機状態なら
            obj->X = obj->X-1;
            if (obj->X < COLS) obj->status = MOVE;// オブジェクトが画面端より右だったら
            obj = obj->next;
        } else obj = obj->next;
    }
    obj = obj->head; // オブジェクトの巻き戻し
}

// プレイヤー位置のオブジェクトが壁か床か判定
char *checkObject(struct PLAYER *ply, struct OBJECT *obj) {
    char *grdwll = "_";
    while (obj->next != NULL) {
        if (obj->X+1 == ply->X) {
            if (obj->objType == WLL) grdwll = "|";
            else grdwll = "_";
        }
        obj = obj->next;
    }
    return grdwll;
}


// ジャンプ処理
void jumpPlayer(struct PLAYER *ply, struct OBJECT *obj, int key) {
    char *body = "^O _|", *grdwll;
    grdwll = checkObject(ply, obj);

    // ジャンプ処理
    if (key == ' ' && ply->status == RUN) {
        mvaddch(ply->Y, ply->X, body[2]);
        mvaddch(ply->Y-1, ply->X, body[0]);
        mvaddch(ply->Y-2, ply->X, body[1]);
        ply->status = UP;
    } else {
        if (ply->status == UP) {
            mvaddch(ply->Y, ply->X, grdwll[0]);
            mvaddch(ply->Y-1, ply->X, body[2]);
            mvaddch(ply->Y-2, ply->X, body[0]);
            mvaddch(ply->Y-3, ply->X, body[1]);
            ply->status = AIR;
        } else if (ply->status == AIR) {
            mvaddch(ply->Y, ply->X, grdwll[0]);
            mvaddch(ply->Y-1, ply->X, body[2]);
            mvaddch(ply->Y-2, ply->X, body[2]);
            mvaddch(ply->Y-3, ply->X, body[0]);
            mvaddch(ply->Y-4, ply->X, body[1]);
            ply->status = DOWN;
        } else if (ply->status == DOWN) {
            mvaddch(ply->Y, ply->X, grdwll[0]);
            mvaddch(ply->Y-1, ply->X, body[2]);
            mvaddch(ply->Y-2, ply->X, body[0]);
            mvaddch(ply->Y-3, ply->X, body[1]);
            mvaddch(ply->Y-4, ply->X, body[2]);
            ply->status = LAND;
        } else {
            ply->status = RUN;
            mvaddch(ply->Y, ply->X, body[0]);
            mvaddch(ply->Y-1, ply->X, body[1]);
            mvaddch(ply->Y-2, ply->X, body[2]);
            mvaddch(ply->Y-3, ply->X, body[2]);
        }
    }
    obj = obj->head; // オブジェクトの巻き戻し
}

// ランキングの表示
void showRanking(int diff) {
    int i,j=0,line=0;
    char c;
    FILE *fp;
    fp = fopen("data.csv", "r");

    // 行数取得
    while ((c=fgetc(fp))!=EOF) if (c == '\n') line++;
    struct RANK rank[line], sort[line], temp;
    rewind(fp);

    // 指定の難易度のデータのみの取得
    for (i=0; i<line; i++) {
        fscanf(fp, "%s %d %d", rank[i].name, &rank[i].score, &rank[i].diff);
        if (rank[i].diff == diff) {
            sort[j] = rank[i];
            j++;
        }
    }

    // データをスコア順にソート
    int len = j;
    for (i=0; i<len; i++) {
        for (j=len-1; j>i; j--) {
            if (sort[j].score > sort[j-1].score) {
                temp = sort[j];
                sort[j] = sort[j-1];
                sort[j-1] = temp;
            }
        }
    }

    // データを出力
		if (len > 5) len = 5;
    for (i=0; i<len; i++) {
        mvprintw(LINES/2-9-5+i, COLS/2, "%s\t\t%d", sort[i].name, sort[i].score);
    }
    mvprintw(LINES/2-10-5, COLS/2, "name\t\tscore");

    fclose(fp);
}

// ゲームオーバー & リトライ
int gameOver(int frm) {
    int flag;
    // メッセージの表示
    mvprintw(LINES/2-7, COLS/2, "GAME OVER!");
    mvprintw(LINES/2-6, COLS/2, "SCORE: %d", frm);
    mvprintw(LINES/2-4, COLS/2, "RETRY?: y");
    mvprintw(LINES/2-3, COLS/2, "QUIT: q");
    refresh();
    // キー入力(y/q)受付
    do {
        timeout(-1);
        flag = getch();
    } while (flag != 'y' && flag != 'q');
    return flag;
}

// 衝突判定
int checkCollision(struct OBJECT *obj, struct PLAYER *ply, int frm) {
    char *grdwll;
    int flag;
    FILE *fp;
    grdwll = checkObject(ply, obj);
    if (ply->status == RUN && grdwll[0] == '|') { // ゲームオーバーの判定
        // データの保存
        fp = fopen("data.csv", "a");
        fprintf(fp, "%s %d %c\n", ply->name, frm, ply->diff);
        fclose(fp);
        showRanking(ply->diff-'0');
        flag = gameOver(frm);
    }
    else flag = 'c';
    return flag;
}

// 難易度選択
int selectDifficulty(struct PLAYER *ply) {
    int waittime;
    erase();
    mvprintw(LINES/2-8, COLS/2-10, "WELCOME TO SCROLL");
    mvprintw(LINES/2-7, COLS/2-10, "SELECT DIFFICULTY");
    mvprintw(LINES/2-6, COLS/2-10, "\tEASY\t: Enter 0");
    mvprintw(LINES/2-5, COLS/2-10, "\tNORMAL\t: Enter 1");
    mvprintw(LINES/2-4, COLS/2-10, "\tHARD\t: Enter 2");
    refresh();
    ply->diff = getch();
    switch (ply->diff) {
        case '0': waittime = 200000;  break;
        case '1': waittime = 100000; break;
        case '2': waittime = 50000; break;
    }
    erase();
    return waittime;
}

int main()
{
    int key, frm=0, flag, waittime;
    struct OBJECT *obj = NULL;
    struct PLAYER *ply;
    // もろもろの初期化
    initscr();
    cbreak();
    curs_set(0);
    keypad(stdscr, TRUE);
    erase();
    // プレイヤーの初期化
    ply = (struct PLAYER *)malloc(sizeof(struct PLAYER));
    initPlayer(ply);
    // 難易度の選択
    waittime = selectDifficulty(ply);
    // スクリーンの初期化
    makeStage(&obj, 1000); // 1000マスのステージ作成
    initStage();

    // メイン処理
    while(1) {
        frm++; // 総フレーム数
        usleep(waittime); // スクロールの遅延
        scrollStage(obj); // ステージのスクロール

        // キー入力チェック
        timeout(0);
        key = getch();
        if (key == 'q') break;
        else jumpPlayer(ply, obj, key);
        mvprintw(3, 3, "NAME : %s", ply->name);
        mvprintw(4, 3, "SCORE: %d", frm);

        // 衝突判定 & ゲームオーバー
        flag = checkCollision(obj, ply, frm);
        if (flag == 'q') break; // ゲーム終了
        else if (flag == 'y') { // リトライ
            // 初期化
            freeStruct(obj, ply);
            obj = NULL;
            ply = (struct PLAYER *)malloc(sizeof(struct PLAYER));
            erase();
            initPlayer(ply);
            waittime = selectDifficulty(ply);
            makeStage(&obj, 1000);
            initStage();
            flag = 'c';
            frm = 0;
        }

        // 画面のリフレッシュ
        refresh();
    }

    // 終了処理
    freeStruct(obj, ply);
    endwin();
    return 0;
}
