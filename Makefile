# Makefile2

OBJS = hello.c

hello: $(OBJS)
	$(CC) -Wall -O2 -lncurses -o hello $(OBJS)

.PHONY: clean
clean:
	$(RM) hello $(objs)
